#!/bin/bash
CWD=$(pwd);
AGENT_TYPE="app"
BACKUPFOLDER="$(date +%Y%m%d_%H%M%S)-${AGENT_TYPE}"
AGENT_VERSION="22.5.0.33845"
APPD_FOLDER="/opt/appdynamics"
AGENT_FOLDER="${APPD_FOLDER}/${AGENT_TYPE}-agent"
AGENT_DOWNLOAD="https://download-files.appdynamics.com/download-file/java-jdk8/${AGENT_VERSION}/AppServerAgent-1.8-${AGENT_VERSION}.zip"

if [[ ! -d "${APPD_FOLDER}" ]]
then
  echo "Appdynamics is not installed in the correct location ${APPD_FOLDER}"
  exit;
fi

# Navigate into AppDynamics folder
if [[ ! -d "${AGENT_FOLDER}" ]]
then
  echo "Agent folder not found ${AGENT_FOLDER}"
  echo "Creathing folder ${AGENT_FOLDER}"
  mkdir -p ${AGENT_FOLDER}
else
  echo "Agent folder found ${AGENT_FOLDER} moving to temp dir ${AGENT_FOLDER}.old"
  mv ${AGENT_FOLDER} ${AGENT_FOLDER}.old
fi

if [[ -d "${AGENT_FOLDER}.old" ]]
then
  cd ${APPD_FOLDER}
  # Make backup folder and make a backup of the configuration
  mkdir -p backup
  mkdir backup/"${BACKUPFOLDER}"
  cp -R ${AGENT_FOLDER}/conf backup/"${BACKUPFOLDER}"
  # Move the machine-agent to a backup folder
  # Create the new folder and download the agent
  mkdir ${AGENT_FOLDER}
  cd ${AGENT_FOLDER}
  curl -OL ${AGENT_DOWNLOAD}
  echo "Extracting"
  unzip -o *.zip > /dev/null
  # Reinstall the old configuration
  echo "yes | cp -ri ${AGENT_FOLDER}.old/conf/* ${AGENT_FOLDER}/conf/"
  yes | cp -ri ${AGENT_FOLDER}.old/conf/* ${AGENT_FOLDER}/conf/
  # Clean up and remove the old version
  cd ${APPD_FOLDER}
  echo "Remove temp agent folder";
  rm -Rf ${AGENT_FOLDER}.old

  # Fix permissions so mule can write to the folder
  echo "Fix permissions"
  chmod -R 777 ${AGENT_FOLDER}

  # Restart the application or agent
  echo "Remember to restart the service"
else
  echo "Installing agent for the first time"
  mkdir -p ${AGENT_FOLDER}
  cd ${AGENT_FOLDER}
  curl -OL "https://download-files.appdynamics.com/download-file/java-jdk8/${AGENT_VERSION}/AppServerAgent-1.8-${AGENT_VERSION}.zip"
  unzip *.zip > /dev/null
  chmod -R 777 ${AGENT_FOLDER}
fi
cd "$CWD"
