#!/bin/bash
CWD=$(pwd);
AGENT_TYPE="database"
BACKUPFOLDER="$(date +%Y%m%d_%H%M)-${AGENT_TYPE}"
APPD_FOLDER="/opt/appdynamics"
AGENT_VERSION="22.5.0.3361"
AGENT_DOWNLOAD="https://download-files.appdynamics.com/download-file/db-agent/${AGENT_VERSION}/db-agent-${AGENT_VERSION}.zip"
if [[ ! -d "${APPD_FOLDER}/${AGENT_TYPE}-agent" ]]
then
  echo "Appdynamics is not installed in the correct location ${APPD_FOLDER}"
  exit;
fi

# Navigate into AppDynamics folder
if [[ ! -d "${APPD_FOLDER}/${AGENT_TYPE}-agent" ]]
then
  echo "Agent folder not found ${APPD_FOLDER}/${AGENT_TYPE}-agent"
  exit;
fi
if [[ -d "${APPD_FOLDER}/${AGENT_TYPE}-agent" ]]
then
  cd ${APPD_FOLDER}
  # Make backup folder and make a backup of the configuration
  mkdir -p backup
  mkdir backup/"$BACKUPFOLDER"
  cp -R ${AGENT_TYPE}-agent/conf backup/"$BACKUPFOLDER"/
  # Move the machine-agent to a backup folder
  mv ${AGENT_TYPE}-agent ${AGENT_TYPE}-agent.old
  # Create the new folder and download the agent
  mkdir ${AGENT_TYPE}-agent
  cd ${AGENT_TYPE}-agent
  curl -OL
  unzip *.zip

  # Reinstall the old configuration
  cp -r ../${AGENT_TYPE}-agent.old/conf/* conf/

  # Clean up and remove the old version
  cd ${APPD_FOLDER}
  rm -R ${AGENT_TYPE}-agent.old

  # Restart the application or agent
  echo "Restarting agent..."
  systemctl restart appdynamics-databaseagent.service
  systemctl status appdynamics-databaseagent.service
fi
cd "$CWD"
