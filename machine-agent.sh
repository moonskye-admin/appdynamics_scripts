#!/bin/bash
CWD=$(pwd);
AGENT_TYPE="machine"
BACKUPFOLDER="$(date +%Y%m%d_%H%M%S)-${AGENT_TYPE}"
AGENT_VERSION="22.5.0.3361"
APPD_FOLDER="/opt/appdynamics"
AGENT_FOLDER="${APPD_FOLDER}/${AGENT_TYPE}-agent"
AGENT_DOWNLOAD="https://download-files.appdynamics.com/download-file/machine-bundle/${AGENT_VERSION}/machineagent-bundle-64bit-linux-${AGENT_VERSION}.zip"

if [[ ! -d "${APPD_FOLDER}" ]]
then
  echo "Appdynamics is not installed in the correct location ${APPD_FOLDER}"
  exit;
fi

# Navigate into AppDynamics folder
if [[ ! -d ${AGENT_FOLDER} ]]
then
  echo "Agent folder not found ${AGENT_FOLDER}"
  echo "Creating folder ${AGENT_FOLDER}"
  mkdir -p "${AGENT_FOLDER}";
else
  echo "Agent folder found, making backup";
  cd ${APPD_FOLDER} || exit
  # Make backup folder and make a backup of the configuration
  mkdir -p backup
  mkdir backup/"$BACKUPFOLDER"
  cp -R ${AGENT_TYPE}-agent/conf backup/"$BACKUPFOLDER"/
  # Create the new folder and download the agent
  mv ${AGENT_FOLDER} ${AGENT_FOLDER}.old
fi

if [[ -d "${AGENT_FOLDER}.old" ]]
then
  cd ${CWD}
  cd ${APPD_FOLDER}
  mkdir -p ${AGENT_FOLDER}
  cd ${AGENT_FOLDER} || exit
  curl -OL ${AGENT_DOWNLOAD}
  echo "Extracting"
  unzip -o *.zip > /dev/null

  # Reinstall the old configuration
  cp -rf ${AGENT_FOLDER}.old/conf/* ${AGENT_FOLDER}/conf/

  # Clean up and remove the old version
  cd ${APPD_FOLDER} || exit
  rm -Rf ${AGENT_TYPE}-agent.old
  if [[ -f "/etc/systemd/system/appdynamics-${AGENT_TYPE}agent.service" ]]
  then
      cd "$CWD"
      cp appdynamics-${AGENT_TYPE}agent.service "/etc/systemd/system/appdynamics-${AGENT_TYPE}agent.service"
      systemctl daemon-reload
  fi
  # Restart the application or agent
  if [[ -f "/etc/systemd/system/appdynamics-${AGENT_TYPE}agent.service" ]]
  then
    echo "Restarting agent...";
    systemctl restart appdynamics-${AGENT_TYPE}agent.service
    systemctl status appdynamics-${AGENT_TYPE}agent.service
  fi
else
  if [[ -d "${AGENT_FOLDER}" ]]
  then
    echo "Installing agent for the first time"
    cd ${AGENT_FOLDER} || exit
      curl -OL "https://download-files.appdynamics.com/download-file/machine-bundle/${AGENT_VERSION}/machineagent-bundle-64bit-linux-${AGENT_VERSION}.zip"
      echo "Extracting"
        unzip *.zip > /dev/null
  fi
fi
echo "Done"
cd "$CWD"


