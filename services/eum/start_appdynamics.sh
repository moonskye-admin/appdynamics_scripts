#!/bin/bash
export JAVA_HOME=/opt/appdynamics/eum/jre
echo "Starting Mysql database"
/opt/appdynamics/eum/orcha/orcha-master/bin/orcha-master -d mysql.groovy -p /opt/appdynamics/eum/orcha/playbooks/mysql-orcha/start-mysql.orcha -o /opt/appdynamics/eum/orcha/orcha-master/conf/orcha.properties -c local
cd /opt/appdynamics/eum/eum-processor
echo "Starting EUM Processor"
/opt/appdynamics/eum/eum-processor/bin/eum-processor